import React, { useState } from "react";
import Box from "@mui/material/Box";

import { Badge, Typography } from "@mui/material";
import { Person, Menu, Upload, FileUpload } from "@mui/icons-material";
import { useWindowDimensions } from "../../routes/dashboard/helper";
import ModalMenu from "../modal/menu";
import ModalProfile from "../modal/profile";

const ResponsiveAppBar = () => {
  const [menuOpen, setMenuOpen] = useState(false);
  const [profileOpen, setProfileOpen] = useState(false);

  const { width } = useWindowDimensions();
  const isMobile = width < 821;

  return (
    <nav className="navbar">
      <div className="nav-content">
        {!isMobile ? (
          <>
            <p className="logo">
              <b>Social</b>Network
            </p>
            <input placeholder="Find..." />
            <button style={{ width: "200px" }} type="button">
              <Upload />
              <Typography subtitle="caption">Upload</Typography>
            </button>
            <Box className="profile">
              <button type="button" onClick={() => setProfileOpen(true)}>
                <Person />
              </button>
              <Box>
                <Badge
                  badgeContent={4}
                  classes={{ badge: { backgroundColor: "#ffe7b3 !important" } }}
                >
                  <Typography variant="subtitle2">Wassem</Typography>
                </Badge>
                <Typography variant="caption">Arshad</Typography>
              </Box>
            </Box>
          </>
        ) : (
          <>
            <div style={{ width: "100px" }}>
              <Menu onClick={() => setMenuOpen(true)} className="pointer" />
            </div>
            <p className="logo">
              <b>Social</b>Network
            </p>
            <div style={{ width: "100px", textAlign: "right" }}>
              <FileUpload style={{ marginRight: "15px" }} />
              <Person onClick={() => setProfileOpen(true)} />
            </div>
          </>
        )}
      </div>
      <ModalMenu open={menuOpen} setOpen={setMenuOpen} />
      <ModalProfile open={profileOpen} setOpen={setProfileOpen} />
    </nav>
  );
};
export default ResponsiveAppBar;
