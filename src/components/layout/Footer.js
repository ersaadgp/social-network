import * as React from "react";

import { useNavigate } from "react-router";
import { useDispatch } from "react-redux";
import { onLogout } from "../../redux/actions/Auth";
import { Box, Typography } from "@mui/material";
import {
  Person,
  Menu,
  Upload,
  FileUpload,
  Twitter,
  LinkedIn,
  Facebook,
} from "@mui/icons-material";
import { useWindowDimensions } from "../../routes/dashboard/helper";

const ResponsiveAppBar = () => {
  const menus = [
    "About",
    "For Business",
    "Suggestions",
    "Help & FAQs",
    "Contact Us",
    "Pricing",
  ];

  const { width } = useWindowDimensions();
  const isMobile = width < 821;

  return (
    <div className="footer">
      <Box className="icon-container">
        <Box className="icon-footer">
          <Twitter fontSize="small" />
        </Box>
        <Box className="icon-footer">
          <LinkedIn fontSize="small" />
        </Box>
        <Box className="icon-footer">
          <Facebook fontSize="small" />
        </Box>
      </Box>
      <div className="sub-footer" style={{ margin: "20px 0px" }}>
        {menus.map((menu, i) => (
          <React.Fragment key={i}>
            <Typography variant="caption" className="pointer">
              {menu}
            </Typography>
            {i !== menus.length - 1 && (
              <Typography variant="caption" style={{ margin: "0px 15px" }}>
                {!isMobile && "/"}
              </Typography>
            )}
          </React.Fragment>
        ))}
      </div>
      <Typography variant="caption" className="secondary">
        © Copyright 2022 companyname inc.
      </Typography>
      <Box className="sub-footer" style={{ marginBottom: "20px" }}>
        <Typography variant="caption">Privacy</Typography>
        <Typography variant="caption" style={{ margin: "0px 15px" }}>
          /
        </Typography>
        <Typography variant="caption">Terms</Typography>
      </Box>
    </div>
  );
};
export default ResponsiveAppBar;
