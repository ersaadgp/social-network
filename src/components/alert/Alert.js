import React, { useEffect } from "react";
import Alert from "@mui/material/Alert";
import Stack from "@mui/material/Stack";
import { useDispatch, useSelector } from "react-redux";
import Backdrop from "@mui/material/Backdrop";
import CircularProgress from "@mui/material/CircularProgress";
import { fetchError } from "../../redux/actions";
import { otpRequest } from "../../redux/actions/Auth";
import { useNavigate } from "react-router";

const style = {
  width: "100%",
  position: "fixed",
  maxWidth: "500px",
  top: "100px",
};

export default function BasicAlerts() {
  const dispatch = useDispatch();
  const nav = useNavigate();
  const { error, message, loading } = useSelector(({ common }) => common);

  useEffect(() => {
    setTimeout(() => {
      dispatch(fetchError(""));
    }, 5000);
  }, [dispatch, error, message]);

  useEffect(() => {
    if (message === "Account Register Success") nav("/otp");
    else if (message === "Verification Success") nav("/login");
    else if (message === "Logout Success") nav("/login");
    else if (error === "Request failed with status code 422") {
      const formData = new FormData();
      formData.append("phone", localStorage.getItem("@phone"));
      dispatch(otpRequest(formData));
      nav("/otp");
    }
  }, [message, error]);

  return (
    <>
      <Stack sx={style} spacing={2}>
        {error && <Alert severity="error">{error}</Alert>}
        {message && <Alert severity="success">{message}</Alert>}
      </Stack>
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={loading}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
    </>
  );
}
