import React, { useState } from "react";
import Backdrop from "@mui/material/Backdrop";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { Close, FileUpload, Logout } from "@mui/icons-material";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "90%",
  maxWidth: "400px",
  color: "rgb(175, 74, 74)",
  p: 1,
  outline: "none",
};

const ModalProfile = ({ open, setOpen }) => {
  const handleClose = () => setOpen(false);
  const [isActive, setActive] = useState(null);

  const menus = ["My Profile", "Edit Profile", "Security"];

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Box sx={style}>
            <Box className="modal-container">
              <Box className="modal-header">
                <img
                  src="https://i.pinimg.com/736x/25/78/61/25786134576ce0344893b33a051160b1.jpg"
                  className="profile-picture"
                />
                <Typography
                  id="transition-modal-title"
                  variant="h6"
                  className="profile-name"
                >
                  Waseem Ahmad
                </Typography>
                <Typography variant="caption">UI/UX Designer</Typography>
                <button className="button-profile ">
                  <FileUpload fontSize="small" />
                  <Typography variant="subtitle2">Start Upload</Typography>
                </button>
              </Box>
              <Box style={{ marginBottom: "100px" }}>
                {menus.map((menu, i) => (
                  <div
                    key={i}
                    onClick={() => setActive(i)}
                    className={`${i == isActive && "menu-active"} modal-menu`}
                  >
                    <Typography variant="body1" display="block">
                      {menu}
                    </Typography>
                  </div>
                ))}
              </Box>
              <button className="btn-logout">
                <Typography variant="subtitle2">Log Out</Typography>
                <Logout fontSize="small" style={{ marginLeft: "10px" }} />
              </button>
            </Box>
            <Box className="modal-close">
              <Close className="modal-close" onClick={handleClose} />
            </Box>
          </Box>
        </Fade>
      </Modal>
    </div>
  );
};

export default ModalProfile;
