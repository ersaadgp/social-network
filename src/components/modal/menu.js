import React, { useState } from "react";
import Backdrop from "@mui/material/Backdrop";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { Close } from "@mui/icons-material";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "90%",
  color: "rgb(175, 74, 74)",
  p: 1,
  outline: "none",
};

const ModalMenu = ({ open, setOpen }) => {
  const handleClose = () => setOpen(false);
  const [isActive, setActive] = useState(null);

  const menus = [
    "Videos",
    "People",
    "Documents",
    "Events",
    "Communities",
    "Favorites",
    "Channels",
  ];

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Box sx={style}>
            <Box className="modal-container">
              <Typography id="transition-modal-title" className="modal-header">
                Main Menu
              </Typography>
              {menus.map((menu, i) => (
                <div
                  key={i}
                  onClick={() => setActive(i)}
                  className={`${i == isActive && "menu-active"} modal-menu`}
                >
                  <Typography variant="body1" display="block">
                    {menu}
                  </Typography>
                </div>
              ))}
            </Box>
            <Box className="modal-close">
              <Close className="modal-close" onClick={handleClose} />
            </Box>
          </Box>
        </Fade>
      </Modal>
    </div>
  );
};

export default ModalMenu;
