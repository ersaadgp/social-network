import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

import Common from "./Common";
import Profile from "./Profile";

export default (history) =>
  combineReducers({
    router: connectRouter(history),
    common: Common,
    profileReducer: Profile,
  });
