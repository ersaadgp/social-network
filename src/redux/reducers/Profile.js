import { GET_PROFILE } from "../types/Profile";

const INIT_STATE = {
  profile: {},
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_PROFILE: {
      return { ...state, profile: action.payload };
    }

    default:
      return state;
  }
};
