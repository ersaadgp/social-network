import { fetchError, fetchStart, fetchSuccess } from ".";
import { GET_PROFILE } from "../types/Profile";
import axios from "axios";
const URL = process.env.REACT_APP_BASE_URL;
const token = localStorage.getItem("@token");

const headers = {
  headers: {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Headers": "*",
    "Content-Type": "application/x-www-form-urlencoded",
    Accept: "application/json",
    Authorization: token,
  },
};

export const getProfile = () => {
  return (dispatch) => {
    dispatch(fetchStart());
    axios
      .get(URL + "profile/me", headers)
      .then((res) => {
        if (res.status === 200) {
          dispatch(fetchSuccess());
          dispatch({ type: GET_PROFILE, payload: res.data });
        } else {
          dispatch(fetchError(res.error));
        }
      })
      .catch(function (error) {
        dispatch(fetchError(error.message));
      });
  };
};

export const createEducation = (payload) => {
  return (dispatch) => {
    dispatch(fetchStart());
    axios
      .post(URL + "profile/education", payload, headers)
      .then((res) => {
        if (res.status === 200) {
          dispatch(fetchSuccess());
          dispatch({ type: GET_PROFILE, payload: res.data });
        } else {
          dispatch(fetchError(res.error));
        }
      })
      .catch(function (error) {
        dispatch(fetchError(error.message));
      });
  };
};
