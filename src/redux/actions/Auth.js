import { fetchError, fetchStart, fetchSuccess } from "../actions";
import axios from "axios";
const URL = process.env.REACT_APP_BASE_URL;
const token = localStorage.getItem("token");
const headers = {
  headers: {
    "Content-Type": "application/x-www-form-urlencoded",
    Accept: "application/json",
  },
};

export const onRegister = (payload) => {
  return (dispatch) => {
    dispatch(fetchStart());
    axios
      .post(URL + "register", payload, headers)
      .then((res) => {
        if (res.status === 201) {
          dispatch(fetchSuccess("Account Register Success"));
          localStorage.setItem("@phone", res.data.data.user.phone);
          localStorage.setItem("@uid", res.data.data.user.id);
        } else {
          dispatch(fetchError(res.error));
        }
      })
      .catch(function (error) {
        dispatch(fetchError(error.message));
      });
  };
};

export const onLogin = (payload) => {
  return (dispatch) => {
    dispatch(fetchStart());
    axios
      .post(URL + "oauth/sign_in", payload, headers)
      .then((res) => {
        if (res.status === 201) {
          dispatch(fetchSuccess("Login Success"));
          localStorage.setItem("@token", res.data.data.user.access_token);
          window.location.reload();
        } else {
          dispatch(fetchError(res.error));
        }
      })
      .catch(function (error) {
        const message = error?.response?.data?.error?.errors[0];
        dispatch(fetchError(message));
      });
  };
};

export const otpVerification = (payload) => {
  return (dispatch) => {
    dispatch(fetchStart());
    axios
      .post(URL + "register/otp/match", payload, headers)
      .then((res) => {
        if (res.status === 201) {
          dispatch(fetchSuccess("Verification Success"));
        } else {
          dispatch(fetchError(res.error));
        }
      })
      .catch(function (error) {
        dispatch(fetchError(error.message));
      });
  };
};

export const otpRequest = (payload) => {
  return (dispatch) => {
    dispatch(fetchStart());
    axios
      .post(URL + "register/otp/request", payload, headers)
      .then((res) => {
        if (res.status === 201) {
          dispatch(fetchSuccess("OTP Sent"));
        } else {
          dispatch(fetchError(res.error));
        }
      })
      .catch(function (error) {
        dispatch(fetchError(error.message));
      });
  };
};

export const onLogout = () => {
  const formData = new FormData();
  formData.append("access_token", localStorage.getItem("@token"));
  formData.append("confirm", 1);
  return (dispatch) => {
    dispatch(fetchStart());
    axios
      .post(URL + "oauth/revoke", formData, headers)
      .then((res) => {
        if (res.status === 201) {
          dispatch(fetchSuccess("Logout Success"));
          localStorage.removeItem("@token");
          window.location.reload();
        } else {
          dispatch(fetchError(res.error));
        }
      })
      .catch(function (error) {
        window.location.reload();
        localStorage.removeItem("@token");
        dispatch(fetchError(error.message));
      });
  };
};
