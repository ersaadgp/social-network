import { useState, useEffect } from "react";
import {
  Comment,
  Favorite,
  LockOpen,
  Send,
  VideoCall,
} from "@mui/icons-material";

export const data = [
  {
    title: "How to improve your skills",
    publisher: "Wassem Arshad",
    viewer: 1231442,
  },
  {
    title: "How to improve your skills",
    publisher: "Michael Krilligh",
    viewer: 123414,
  },
  {
    title: "How to improve your skills",
    publisher: "Ahmad Yasin",
    viewer: 543333,
  },
  {
    title: "How to improve your skills",
    publisher: "John Stoiner",
    viewer: 5363243,
  },
  {
    title: "How to improve your skills",
    publisher: "John Doe",
    viewer: 4743563,
  },
  {
    title: "How to improve your skills",
    publisher: "John Stoiner",
    viewer: 5363243,
  },
];

export const people = [
  {
    title: "Wassem Arshad",
    subtitle: "UI Desiner",
  },
  {
    title: "James Labri",
    subtitle: "Frontend Dev.",
  },
  {
    title: "Ahmad Yasin",
    subtitle: "Backend Dev.",
  },
  {
    title: "John Stoiner",
    subtitle: "QA Engineer",
  },
  {
    title: "John Doe",
    subtitle: "Project Manager",
  },
  {
    title: "John Doe",
    subtitle: "Project Manager",
  },
];

export const documents = [
  {
    title: "Mobile UI/UX Guide 2013",
    subtitle: "UI Desiner",
    viewer: 12345,
  },
  {
    title: "HTML & CSS",
    subtitle: "Frontend Dev.",
    viewer: 33422,
  },
  {
    title: "Basic Javascript",
    subtitle: "Backend Dev.",
    viewer: 5223,
  },
  {
    title: "Unit Test with Jest",
    subtitle: "QA Engineer",
    viewer: 5225,
  },
  {
    title: "How to Manage Your Team",
    subtitle: "Project Manager",
    viewer: 1351,
  },
  {
    title: "How to Manage Your Team",
    subtitle: "Project Manager",
    viewer: 1351,
  },
];

export const activity = [
  {
    title: "How to improve your skills",
    name: "Wassem Arshad",
    date: "2 weeks ago",
    action: 0,
  },
  {
    title: "How to improve your skills",
    name: "Michael Krilligh",
    date: "2 weeks ago",
    action: 1,
  },
  {
    title: "How to improve your skills",
    name: "Ahmad Yasin",
    date: "2 weeks ago",
    action: 2,
  },
  {
    title: "How to improve your skills",
    name: "John Stoiner",
    date: "2 weeks ago",
    action: 3,
  },
  {
    title: "How to improve your skills",
    name: "John Doe",
    date: "2 weeks ago",
    action: 4,
  },
  {
    title: "How to improve your skills",
    name: "John Doe",
    date: "2 weeks ago",
    action: 5,
  },
];

export const channel = [
  "Google",
  "Dribble",
  "Microsoft",
  "Belhance",
  "Weather Channel",
  "Gurus",
  "iMedia",
  "Creativeye",
  "Khan Studios",
  "Yahoo",
];

export const actionType = [
  {
    icon: <Comment fontSize="small" style={{ marginRight: "5px" }} />,
    activity: "commented",
  },
  {
    icon: <VideoCall fontSize="small" style={{ marginRight: "5px" }} />,
    activity: "added a new video",
  },
  {
    icon: <Send fontSize="small" style={{ marginRight: "5px" }} />,
    activity: "shared a video",
  },
  {
    icon: <LockOpen fontSize="small" style={{ marginRight: "5px" }} />,
    activity: "unlock a video",
  },
  {
    icon: <VideoCall fontSize="small" style={{ marginRight: "5px" }} />,
    activity: "uploaded a video",
  },
  {
    icon: <Favorite fontSize="small" style={{ marginRight: "5px" }} />,
    activity: "liked a video",
  },
];

export const settings = {
  dots: false,
  infinite: false,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
};

export const settings2 = {
  dots: false,
  infinite: false,
  speed: 500,
  slidesToShow: 2,
  slidesToScroll: 1,
  arrows: false,
};

const getWindowDimensions = () => {
  const { innerWidth: width, innerHeight: height } = window;
  return {
    width,
    height,
  };
};

export const useWindowDimensions = () => {
  const [windowDimensions, setWindowDimensions] = useState(
    getWindowDimensions()
  );

  useEffect(() => {
    function handleResize() {
      setWindowDimensions(getWindowDimensions());
    }

    window.addEventListener("resize", handleResize);

    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return windowDimensions;
};
