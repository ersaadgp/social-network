import React, { useEffect, useState } from "react";
import { Grid, Typography, Box } from "@mui/material";
import Header from "./components/header";
import Boxes from "./components/box";
import List from "./components/activityList";
import Channels from "./components/channelList";
import Videos from "./sections/videos";
import {
  data,
  people,
  documents,
  activity,
  channel,
  useWindowDimensions,
  settings,
} from "./helper";
import Slider from "react-slick";
import People from "./sections/people";
import Documents from "./sections/documents";
import { ArrowForward } from "@mui/icons-material";

const Dashboard = (props) => {
  const [videos, setVideos] = useState([]);
  const [activities, setActivities] = useState([]);
  const [channels, setChannels] = useState([]);

  const [actActive, setActActive] = useState(0);
  const [chActive, setChActive] = useState(0);

  const { width } = useWindowDimensions();
  const isMobile = width < 821;

  const generateNewData = () => {
    let newChannels = [];
    let newActivities = [];

    for (let i = 0; i < 9; i++) {
      newChannels.push(channel[i]);
    }

    for (let i = 0; i < 4; i++) {
      newActivities.push(activity[i]);
    }

    setActivities(newActivities);
    setChannels(newChannels);
  };

  useEffect(() => {
    setVideos(data);

    if (!isMobile) {
      setActivities(activity);
      setChannels(channel);
    } else {
      generateNewData();
    }
  }, [isMobile]);

  return (
    <>
      <Grid container spacing={3} style={{ marginTop: "0 !important" }}>
        <Grid item xs={12}>
          {!isMobile ? <Header /> : <input placeholder="Find..." />}
        </Grid>

        {/* For Videos */}
        {isMobile ? (
          <Grid item xs={12} className="videos">
            <div className="title-side">
              <Typography variant="h5">Videos</Typography>
              <Typography variant="caption" className="pointer" display="flex">
                Browse all videos
                {isMobile && <ArrowForward fontSize="small" />}
              </Typography>
            </div>
            <Slider {...settings}>
              {videos.map((row, i) => (
                <div style={{ padding: "15px !important" }} key={i}>
                  <Boxes data={row} isMobile={isMobile} />
                </div>
              ))}
            </Slider>
          </Grid>
        ) : (
          <Grid item lg={8} md={12}>
            <div className="title">
              <Typography variant="h5">Videos</Typography>
              <Typography variant="caption" className="pointer" display="flex">
                Browse all videos
                {isMobile && <ArrowForward fontSize="small" />}
              </Typography>
            </div>
            <Videos data={videos} />
          </Grid>
        )}

        {/* For Activity */}
        <Grid item lg={4} xs={12}>
          <div className="title-side">
            <Typography variant="h5">Activity</Typography>
            <Typography variant="caption" className="pointer" display="flex">
              View timeline / Filter activities
              {isMobile && <ArrowForward fontSize="small" />}
            </Typography>
          </div>
          <div className="brake-top"></div>
          <Grid container>
            {activities.map((row, i) => (
              <Grid item lg={12} md={6} xs={12} key={i}>
                <List
                  index={i}
                  data={row}
                  isActive={i === actActive}
                  setActive={setActActive}
                />
              </Grid>
            ))}
          </Grid>
          <div className="brake-bottom"></div>
        </Grid>

        <Grid item lg={8} xs={12} className="custom">
          {/* For People */}
          <People data={people} isMobile={isMobile} />

          {/* For Documents Desktop */}
          {!isMobile && <Documents data={documents} />}
        </Grid>

        {/* For Channels */}
        <Grid item lg={4} xs={12}>
          <div className="title-side">
            <Typography variant="h5">Channels</Typography>
            <Typography variant="caption" className="pointer" display="flex">
              Browse all channels
              {isMobile && <ArrowForward fontSize="small" />}
            </Typography>
          </div>
          <div className="brake-top"></div>
          <Grid container spacing={1}>
            {channels.map((row, i) => (
              <Grid item lg={6} xs={4} md={6} key={i}>
                <Channels
                  index={i}
                  data={row}
                  isActive={i === chActive}
                  setActive={setChActive}
                />
              </Grid>
            ))}
          </Grid>
          <div className="brake-bottom" style={{ marginTop: "10px" }}></div>
        </Grid>

        {/* For Documents Desktop */}
        {isMobile && <Documents data={documents} isMobile={isMobile} />}
      </Grid>
    </>
  );
};

export default Dashboard;
