import React from "react";

import { Typography, Box } from "@mui/material";

const Dashboard = (props) => {
  const { data, isMain, isMobile } = props;

  const formatViewer = (value) => {
    let newValue = value.toString().slice(0, -3);

    return isMobile ? `${newValue}k` : value;
  };

  return (
    data && (
      <div className="block">
        <div className="block-content">
          {(isMain || isMobile) && (
            <Typography variant="h6">{data.title}</Typography>
          )}
          <Box className={!isMobile && `d-flex-sb`}>
            <Typography variant="caption" display="block">
              {data.publisher ?? data.subtitle}
            </Typography>
            {data?.viewer && (
              <Typography variant="caption">
                <b>{formatViewer(data?.viewer)} Views</b>
              </Typography>
            )}
          </Box>
        </div>
      </div>
    )
  );
};

export default Dashboard;
