import { Typography } from "@mui/material";
import React from "react";

const Header = () => {
  const menus = [
    "Videos",
    "People",
    "Documents",
    "Events",
    "Communities",
    "Favorites",
    "Channels",
  ];

  return (
    <div className="header">
      {menus.map((menu, i) => (
        <React.Fragment key={i}>
          <Typography variant="caption" className="pointer">
            {menu}
          </Typography>
          {i !== menus.length - 1 && (
            <Typography variant="caption" style={{ margin: "0px 15px" }}>
              /
            </Typography>
          )}
        </React.Fragment>
      ))}
    </div>
  );
};

export default Header;
