import React from "react";

import { Typography } from "@mui/material";

const Dashboard = (props) => {
  const { title, icon } = props;

  return (
    <>
      <div className="block-border">
        <div style={{ marginRight: "5px" }}>{icon}</div>
        <Typography variant="subtitle2"> {title}</Typography>
      </div>
    </>
  );
};

export default Dashboard;
