import React from "react";
import { Typography, Box, Grid } from "@mui/material";
import { actionType } from "../helper";
import { Add, Close } from "@mui/icons-material";

const img =
  "https://dl.fujifilm-x.com/global/products/lenses/xf70-300mmf45-56-r-lm-ois-wr/sample-images/xf70-300mmf45-56-r-lm-ois-wr_sample_06_hunk.jpg";

const Channel = (props) => {
  const { data, setActive, isActive, index } = props;

  const handleClick = (e) => {
    e.stopPropagation();
    setActive(index);
  };

  return (
    data && (
      <div
        className={`grid ${isActive && "grid-active"}`}
        onClick={handleClick}
      >
        <Typography>{data}</Typography>
        <Add className="close-corner" style={{ color: "rgb(255, 231, 179)" }} />
      </div>
    )
  );
};

export default Channel;
