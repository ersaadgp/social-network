import React from "react";
import { Typography, Box, Grid } from "@mui/material";
import { actionType } from "../helper";
import { Close } from "@mui/icons-material";

const img =
  "https://dl.fujifilm-x.com/global/products/lenses/xf70-300mmf45-56-r-lm-ois-wr/sample-images/xf70-300mmf45-56-r-lm-ois-wr_sample_06_hunk.jpg";

const Dashboard = (props) => {
  const { data, setActive, isActive, index } = props;

  const handleClick = (e) => {
    e.stopPropagation();
    setActive(index);
  };

  return (
    <div className={`list ${isActive && "active"}`} onClick={handleClick}>
      {data && (
        <Grid container spacing={1}>
          <Grid item xs={3}>
            <img src={img} />
          </Grid>
          <Grid item xs={9}>
            <span>
              <b>{data.name} </b>
            </span>
            <span>{actionType[data.action].activity}</span>
            <Typography variant="caption" display="block">
              {data.title}
            </Typography>
            <Typography variant="caption" display="flex" alignItems="center">
              {actionType[data.action].icon} {data.date}
            </Typography>
          </Grid>
        </Grid>
      )}
      <Close className="close-corner" />
    </div>
  );
};

export default Dashboard;
