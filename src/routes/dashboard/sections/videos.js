import React, { useEffect, useState } from "react";
import { Grid, Typography } from "@mui/material";
import Box from "../components/box";
import Upload from "../components/uploadBox";
import { data, activity, channel, useWindowDimensions } from "../helper";
import { FileUpload } from "@mui/icons-material";

const Videos = ({ data }) => {
  return (
    <Grid container spacing={1}>
      <Grid item md={8}>
        <Box data={data[0]} isMain />
      </Grid>
      <Grid item md={4}>
        <Grid container spacing={1}>
          <Grid item md={12}>
            <Box data={data[1]} />
          </Grid>
          <Grid item md={12}>
            <Box data={data[2]} />
          </Grid>
        </Grid>
      </Grid>
      <Grid item md={12}>
        <Grid container spacing={1}>
          <Grid item md={4}>
            <Box data={data[3]} />
          </Grid>
          <Grid item md={4}>
            <Box data={data[4]} />
          </Grid>
          <Grid item md={4}>
            <Upload title="Upload Your Own Video" icon={<FileUpload />} />
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Videos;
