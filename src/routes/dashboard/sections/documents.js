import React, { useEffect, useState } from "react";
import { Grid, Typography, Box as BoxMUI } from "@mui/material";
import Box from "../components/box";
import Upload from "../components/uploadBox";
import { AddCircleOutline, ArrowForward } from "@mui/icons-material";
import Slider from "react-slick";
import { settings2 as settings } from "../helper";

const Documents = ({ data, isMobile }) => {
  return isMobile ? (
    <Grid item xs={12} className="custom">
      <BoxMUI className="title-side">
        <Typography variant="h5">Documents</Typography>
        <Typography variant="caption" className="pointer" display="flex">
          Browse all documents
          {isMobile && <ArrowForward fontSize="small" />}
        </Typography>
      </BoxMUI>
      <Slider {...settings}>
        {data.map((row, i) => (
          <div style={{ padding: "15px !important" }} key={i}>
            <Box data={row} isMobile={isMobile} />
          </div>
        ))}
      </Slider>
    </Grid>
  ) : (
    <>
      <BoxMUI className="title" style={{ marginTop: "20px" }}>
        <Typography variant="h5">Documents</Typography>
        <Typography variant="caption" className="pointer">
          Browse all documents
        </Typography>
      </BoxMUI>
      <Grid container spacing={1}>
        <Grid item md={8}>
          <Box data={data[0]} isMain />
        </Grid>
        <Grid item md={4}>
          <Grid container spacing={1}>
            <Grid item md={12}>
              <Box data={data[1]} />
            </Grid>
            <Grid item md={12}>
              <Box data={data[2]} />
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={12}>
          <Grid container spacing={1}>
            <Grid item md={4}>
              <Box data={data[3]} />
            </Grid>
            <Grid item md={4}>
              <Box data={data[4]} />
            </Grid>
            <Grid item md={4}>
              <Upload title="Share Your Document" icon={<AddCircleOutline />} />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export default Documents;
