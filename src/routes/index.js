import React from "react";
import { Route, Routes } from "react-router";

import Navbar from "../components/layout/Navbar";
import Footer from "../components/layout/Footer";
import Alert from "../components/alert/Alert";
import { useLocation } from "react-router-dom";
import { Container } from "@mui/material";
import Dashboard from "./dashboard";

const Layout = () => {
  const location = useLocation();
  const token = localStorage.getItem("@token");

  // if (
  //   location.pathname === "" ||
  //   location.pathname === "/" ||
  //   (!["/login", "/register", "/otp"].includes(location.pathname) && !token)
  // ) {
  //   return <Navigate to={"/login"} />;
  // } else if (token && location.pathname === "/login") {
  //   return <Navigate to={"/home"} />;
  // }

  return (
    <React.Fragment>
      <Navbar />
      <Container className="container">
        <Alert />
        <Routes>
          <Route path="/" element={<Dashboard />} />
        </Routes>
        <Footer />
      </Container>
    </React.Fragment>
  );
};

export default Layout;
